package org.unistuttgart.quantumcircuit;

import java.util.ArrayList;
import java.util.List;

public class experiments {
	private experimentsConfig config;
	private experimentsHeader header;
	private List<Object> instructions;
	
	experiments(int memorySlots, int qbits, int sizeCreg, int sizeQreg, String name){
		this.config = new experimentsConfig(memorySlots, qbits, false);
		this.header = new experimentsHeader(memorySlots, qbits, name);
		this.instructions = new ArrayList<Object>();
	}


	public experimentsConfig getConfig() {
		return config;
	}


	public void setConfig(experimentsConfig config) {
		this.config = config;
	}


	public experimentsHeader getHeader() {
		return header;
	}


	public void setHeader(experimentsHeader header) {
		this.header = header;
	}
	
	
	public List<Object> getInstructions() {
		return instructions;
	}


	public void setInstructions(List<Object> instructions) {
		this.instructions = instructions;
	}


	public void addQubitInstruction(List<Object> list, String instruction, int[] qubits) {
		instructionsQubit instr = new instructionsQubit(instruction, qubits);
		list.add(instr);
	}
	
	public void addMeasureInstruction(List<Object> list, int[] memory, int[] qubits) {
		instructionsMeasure instr = new instructionsMeasure(memory, qubits);
		list.add(instr);
	}
	
}
