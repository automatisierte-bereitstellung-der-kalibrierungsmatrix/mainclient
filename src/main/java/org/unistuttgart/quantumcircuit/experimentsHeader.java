package org.unistuttgart.quantumcircuit;

public class experimentsHeader {
	String name;
	int memory_slots;
	int n_qubits;
	String[][] qreg_sizes;
	String [][] creg_sizes;
	String [][] qubit_labels;
	String [][] clbit_labels;
	
	
	experimentsHeader(int memorySlots, int qbits, String name){
		this.name = name;
		this.memory_slots = memorySlots;
		this.n_qubits = qbits;
		this.qreg_sizes = new String[1][2];
		this.creg_sizes = new String[1][2];
		this.qubit_labels = new String[qbits][2];
		this.clbit_labels = new String[memorySlots][2];		
	}
	

	public String[][] getClbit_labels() {
		return clbit_labels;
	}

	public void setClbit_labels(String[][] clbit_labels) {
		this.clbit_labels = clbit_labels;
	}

	public String[][] getCreg_sizes() {
		return creg_sizes;
	}

	public void setCreg_sizes(String[][] creg_sizes) {
		this.creg_sizes = creg_sizes;
	}

	public int getMemory_slots() {
		return memory_slots;
	}

	public void setMemory_slots(int memory_slots) {
		this.memory_slots = memory_slots;
	}

	public int getN_qubits() {
		return n_qubits;
	}

	public void setN_qubits(int n_qbits) {
		this.n_qubits = n_qbits;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String[][] getQreg_sizes() {
		return qreg_sizes;
	}

	public void setQreg_sizes(String[][] qreg_sizes) {
		this.qreg_sizes = qreg_sizes;
	}

	public String[][] getQubit_labels() {
		return qubit_labels;
	}

	public void setQubit_labels(String[][] qubit_labels) {
		this.qubit_labels = qubit_labels;
	}
	
	public void setClbitLabelsContent(int memorySlots) {
		for(int i = 0; i < memorySlots; i++) {
			for(int j = 0; j < 2; j++) {
				if(j == 0) {
					clbit_labels[i][j] = "cr";					
				} else {
					clbit_labels[i][j] = Integer.toString(i);
				}
				 
			}
		}
	}
	
	public void setCregSizesContent(int sizeCreg) {
		creg_sizes[0][0] = "cr";
		creg_sizes[0][1] = Integer.toString(sizeCreg);
	}
		
	public void setQubitLabelsContent(int qb) {
		for(int i = 0; i < qb; i++) {
			for(int j = 0; j < 2; j++) {
				if(j == 0) {
					qubit_labels[i][j] = "qr";					
				} else {
					qubit_labels[i][j] = Integer.toString(i);
				}
				 
			}
		}
	}
	
	public void setQregSizesContent(int sizeQreg) {
		qreg_sizes[0][0] = "qr";
		qreg_sizes[0][1] = Integer.toString(sizeQreg);
	}
	
	public void setContent(int memorySlots, int sizeCreg, int qb, int sizeQreg) {
		setClbitLabelsContent(memorySlots);
		setCregSizesContent(sizeCreg);
		setQubitLabelsContent(qb);
		setQregSizesContent(sizeQreg);
	}
	
}
