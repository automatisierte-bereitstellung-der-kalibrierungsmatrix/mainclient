package org.unistuttgart.quantumcircuit;

public class experimentsConfig {
	int memory_slots;
	int n_qubits;
	boolean init_qubits;
	
	
	experimentsConfig(int memory_slots, int n_qubits, boolean init_qubits){
		this.memory_slots = memory_slots;
		this.n_qubits = n_qubits;
		this.init_qubits = init_qubits;
	}

	public int getMemory_slots() {
		return memory_slots;
	}

	public void setMemory_slots(int memory_slots) {
		this.memory_slots = memory_slots;
	}

	public int getN_qubits() {
		return n_qubits;
	}

	public void setN_qubits(int n_qubits) {
		this.n_qubits = n_qubits;
	}

	public boolean isInit_qubits() {
		return init_qubits;
	}

	public void setInit_qubits(boolean init_qubits) {
		this.init_qubits = init_qubits;
	}
	
	
}
