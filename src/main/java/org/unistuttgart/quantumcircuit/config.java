package org.unistuttgart.quantumcircuit;

public class config {
	//class variables
	int memory_slots;
	int n_qubits;
	int shots;
	//easy constructor
	config(int slots, int qbits){
		this.memory_slots = slots;
		this.n_qubits = qbits;
		this.shots = 1024;
	}
	//constructor with number of shots
	config(int slots, int qbits, int shots){
		this.memory_slots = slots;
		this.n_qubits = qbits;
		this.shots = shots;
	}
	
	//Getter and Setters
	public int getMemory_slots() {
		return memory_slots;
	}
	public void setMemory_slots(int memory_slots) {
		this.memory_slots = memory_slots;
	}
	public int getN_qubits() {
		return n_qubits;
	}
	public void setN_qubits(int n_qubits) {
		this.n_qubits = n_qubits;
	}
	
	public int getShots() {
		return shots;
	}
	public void setShots(int shots) {
		this.shots = shots;
	}
	
}
