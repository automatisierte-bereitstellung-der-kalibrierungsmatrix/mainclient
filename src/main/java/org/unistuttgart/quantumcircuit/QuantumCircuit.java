package org.unistuttgart.quantumcircuit;

import java.util.ArrayList;
import java.util.List;

import org.unistuttgart.acaliqu.MyMath;

public class QuantumCircuit {
	//Class variables	
	private String qobj_id;
	private String schema_version;
	private String type;
	private List<experiments> experiments;
	private config config;
	private header header;
	
	//Constructor
	public QuantumCircuit(){
		
	}

	//Method to create a Calibration Circuit for one Element of the Matrix with the desired number of qbits and the column of the calibration matrix to calibrate
	//Each column indicates wich circuit eg. column 0 is C_0, column 1 is C_1,...
	public QuantumCircuit createCalibrationCircuit(QuantumCircuit circuit, String backendName, String backendVersion, int qb, int shots, int column) {
		int memorySlots = qb;
		int sizeCreg = qb;
		int sizeQreg = qb;
		
		String experimentName = "Kalibrierung Schritt " + column;
		
		config = new config(memorySlots, qb, shots);
		experiments = new ArrayList<experiments>();
		
		//Mit memory_slots sind die in dem experiment verwendeten measurment slots gemeint, also die Anzahl der c_reg
		experiments exp = new experiments(memorySlots, qb, sizeCreg, sizeQreg, experimentName);
		//sets the content of the experiment header
		exp.getHeader().setContent(memorySlots, sizeCreg, qb, sizeQreg);
		//get binary representation of the column starting with 2^0, 2^1,...
		MyMath math = new MyMath();
		int[] binary = math.convertDecToBin(column);
		int length = binary.length;
		
		for(int i = 0; i < length; i++) {
			//add an not instruction to every marked qbit (marked by column)
			if(binary[i] == 1) {
				//for a 1 qbit operation qubits has to be size 1
				
				int[] qubits = new int[1];
				qubits[0] = i;
				
				//int qubits = i;
				exp.addQubitInstruction(exp.getInstructions(), "x", qubits);
			}
		}

		for(int j = 0; j < qb; j++) {
			//for a 1 qbit measurment memory has to be size 1
			int[] memory = new int[1];
			memory[0] = j;
			//int qubit = j;
			//qbit 0 will be measured and saved to cbit 0, qbit 1 to cbit 1 etc.
			exp.addMeasureInstruction(exp.getInstructions(), memory, memory);
		}
		experiments.add(exp);
		header = new header(backendName, backendVersion);
		qobj_id = experimentName;
		schema_version = "1.4.0";
		type = "QASM";
		return circuit;
	}
	
	public config getConfig() {
		return config;
	}

	public void setConfig(config config) {
		this.config = config;
	}

	public List<experiments> getExperiments() {
		return experiments;
	}

	public void setExperiments(List<experiments> experiments) {
		this.experiments = experiments;
	}

	public header getHeader() {
		return header;
	}

	public void setHeader(header header) {
		this.header = header;
	}

	public String getQobj_id() {
		return qobj_id;
	}

	public void setQobj_id(String qobj_id) {
		this.qobj_id = qobj_id;
	}

	public String getSchema_version() {
		return schema_version;
	}

	public void setSchema_version(String schema_version) {
		this.schema_version = schema_version;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	
	
}
