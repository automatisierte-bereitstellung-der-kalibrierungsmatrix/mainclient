package org.unistuttgart.quantumcircuit;

/**
 * Class to create a measure instruction
 * it consists of:
 * - a memory array to indicate the place(bits) to store the info
 * - a name which must always be "measure"
 * - a qubits array to indicate which qubits to measure
 */
public class instructionsMeasure {
	int[] qubits;
	int[] memory;
	String name = "measure";
	
	
	instructionsMeasure(int[] memory, int[] qubits){
		this.qubits = qubits;
		this.memory = memory;		
	}
	
	//Getters and Setters
	public int[] getMemory() {
		return memory;
	}

	public void setMemory(int[] memory) {
		this.memory = memory;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int[] getQubits() {
		return qubits;
	}

	public void setQubits(int[] qubits) {
		this.qubits = qubits;
	}
	
}
