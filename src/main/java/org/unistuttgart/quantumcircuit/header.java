package org.unistuttgart.quantumcircuit;

public class header {
	String backend_name;
	String backend_version;
	
	header(String backend_name, String backend_version){
		this.backend_name = backend_name;
		this.backend_version = backend_version;
	}

	public String getBackend_name() {
		return backend_name;
	}

	public void setBackend_name(String backend_name) {
		this.backend_name = backend_name;
	}

	public String getBackend_version() {
		return backend_version;
	}

	public void setBackend_version(String backend_version) {
		this.backend_version = backend_version;
	}
	
}
