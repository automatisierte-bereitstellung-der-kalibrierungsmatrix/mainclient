package org.unistuttgart.quantumcircuit;

/**
 * Class to create a qubit instruction
 * it consists of:
 * - a name which indicates the operation to be used
 * - a qubits array to indicate which qubits the operation is performed on
 */
public class instructionsQubit {
	String name;
	int[] qubits;
	
	
	
	instructionsQubit(String name, int[] qubits){
		this.name = name;
		this.qubits = qubits;		
	}
	
	//Getters and Setters
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int[] getQubits() {
		return qubits;
	}

	public void setQubits(int[] qubits) {
		this.qubits = qubits;
	}
	
	
}
