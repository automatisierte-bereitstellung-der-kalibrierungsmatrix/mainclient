package org.unistuttgart.unfolding;

public class Unfolding {
	
	public Unfolding() {
		
	}
	
	/**
	 * applies an unfolding method determined by the String 
	 * 
	 * @param calibrationMatrix
	 * @param unfoldingMethod
	 * @param nColumns
	 * @return changed calibrationMatrix
	 */
	public double[][] applyUnfoldingMethod(double[][] calibrationMatrix, String unfoldingMethod, int nColumns){
		//choose Unfolding class and method depending on the unfoldingMethod variable
		if(unfoldingMethod.equals("MatrixInversion")) {
			MatrixInversion method = new MatrixInversion();
			calibrationMatrix = method.applyMatrixInversion(calibrationMatrix, nColumns);
		} /*else if(unfoldingMethod.equals("UnfoldingMethod")) {
			//APPLY OTHER UNFOLDING METHOD
		}*/ else {
			//CASE FOR WRONG STRING
		}
		return calibrationMatrix;
	}
}
