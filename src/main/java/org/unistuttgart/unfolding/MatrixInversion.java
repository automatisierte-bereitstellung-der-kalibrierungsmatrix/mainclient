package org.unistuttgart.unfolding;

import org.apache.commons.math3.linear.*;

public class MatrixInversion {
	
	public MatrixInversion() {
		
	}
	
	/**
	 * applies a Matrix Inversion using LU Decomposition (It is not checked if the Matrix is invertible)
	 * 
	 * @param calibrationMatrix
	 * @param nColumns
	 * @return result Matrix
	 */
	public double[][] applyMatrixInversion(double[][] calibrationMatrix, int nColumns) {
		double[][] result = new double[nColumns][nColumns];
		//creates the Column of the Identity Matrix and fills it with 0s
		double[] idCol = new double[nColumns];
		for(int x = 0; x < nColumns; x++) {
			idCol[x] = 0;
		}
		//creates a RealMatrix from the matrix
		RealMatrix a = new Array2DRowRealMatrix(calibrationMatrix);
		//creates a solver wich has the LU Decomposition of the matrix
		DecompositionSolver solver = new LUDecomposition(a).getSolver();
		//Calculates the solution vector x for each column of the result matrix
		for(int i = 0; i < nColumns; i++) {
			//Adjusts the Column of the Identity matrix to fit
			if(i == 0) {
				idCol[i] = 1;
			} else {
				idCol[i] = 1;
				idCol[i-1] = 0;
			}
			//creates a RealVector from the array
			RealVector b = new ArrayRealVector(idCol);
			//Solves the Equation Ax=b
			RealVector x = solver.solve(b);
			//writes the result vector into the result array
			for(int j = 0; j < x.getDimension(); j++) {
				result[j][i] = x.getEntry(j);
			}
		}
		return result;
	}
}
