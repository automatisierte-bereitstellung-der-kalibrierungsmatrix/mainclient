package org.unistuttgart.acaliqu;

public class MyMath {
	public MyMath(){
		
	}
	
	/**
	 * calculates the binary of a decimal number and returns it in order (2^0 2^1 2^2...) inside an int[]
	 * @param decimal
	 * @return int[]
	 */
	public int[] convertDecToBin(int decimal) {
		String bin="";
		int binary[];
		int rest;
		int temp;
		int size;
		if(decimal == 0) {
			binary = new int[1];
			binary[0] = 0;
			return binary;
		}
		while(decimal > 0) {
			temp = decimal/2;
			rest = decimal%2;
			bin += Integer.toString(rest) + ";";
			decimal = temp;
		}
		String[] split = bin.split(";");
		size = split.length;
		binary = new int[size];
		for(int i = size; i > 0; i--) {
			binary[i-1] = Integer.parseInt(split[i-1]);
		}
		return binary;
	}
}
