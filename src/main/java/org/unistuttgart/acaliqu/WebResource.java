package org.unistuttgart.acaliqu;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Objects;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;

import org.glassfish.jersey.client.ClientConfig;
import org.unistuttgart.quantumcircuit.QuantumCircuit;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class WebResource {
//Uri's for the matrixServer as well as IBM Quantum Experience API
private static String urlMatrixServer = "http://localhost:8080/MatrixServer/rest/qc";
private static String urlIBMQuantum = "https://api.quantum-computing.ibm.com/v2";

//Standard values for IBM Quantum Experience paths
private static String standardHubName = "ibm-q";
private static String standardGroupName = "open";
private static String standardProjectName = "main";

//IBM User Token (generated in IBM Quantum Experience
private String userToken = "";

//Path to the qObject.json
private static String qObjectPath = "";

//Getters and Setters

public String getUserToken() {
	return userToken;
}

public void setUserToken(String token) {
	this.userToken = token;
}

//Methods for the MatrixServer

	public WebTarget getServerTarget() {
		//Sets up the url and web target for the server
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		//For more specific resources (e.g. http://localhost:8080/MatrixServer/rest/qc/resource use:
		// WebTarget resourceTarget = webTarget.path("resource");
		return client.target(urlMatrixServer);
	}
	
//WebTargets for the IBM Quantum Experience API
	
	public WebTarget getIBMTarget() {
		//Sets up the url and web target for the server
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		return client.target(urlIBMQuantum);
	}
	
	public String getIBMAPIToken() {
		String token = "{\"apiToken\":\"" + userToken + "\"}";
		
		return token;
	}
	
	public WebTarget getIBMProviderInformationPath(String ibmId) {
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(urlIBMQuantum+"/Network?access_token="+ibmId);
		return target;
	}
	
	public WebTarget getIBMJobObjectStoragePath(String hubName, String groupName, String projectName, String ibmId) {
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(urlIBMQuantum+"/Network/"+hubName+"/Groups/"+groupName+"/Projects/"+projectName+"/Jobs?access_token="+ibmId);
		return target;
	}
	
	public WebTarget getIBMJobUploadUrlPath(String hubName, String groupName, String projectName, String jobId, String ibmId) {
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(urlIBMQuantum+"/Network/"+hubName+"/Groups/"+groupName+"/Projects/"+projectName+"/Jobs/"+jobId+"/jobUploadUrl?access_token="+ibmId);
		return target;		
	}
	
	public WebTarget getIBMDevicesOfProjectInfoPath(String hubName, String groupName, String projectName, String ibmId) {
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(urlIBMQuantum+"/Network/"+hubName+"/Groups/"+groupName+"/Projects/"+projectName+"/devices/v/1?access_token="+ibmId);
		return target;
	}
	
	public WebTarget getIBMJobDataUploadedPath(String hubName, String groupName, String projectName, String ibmId, String jobId) {
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(urlIBMQuantum+"/Network/"+hubName+"/Groups/"+groupName+"/Projects/"+projectName+"/Jobs/"+jobId+"/jobDataUploaded?access_token="+ibmId);
		return target;
	}
	
	public WebTarget getIBMJobByIdPath(String hubName, String groupName, String projectName, String ibmId, String jobId) {
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(urlIBMQuantum+"/Network/"+hubName+"/Groups/"+groupName+"/Projects/"+projectName+"/Jobs/"+jobId+"/v/1?access_token="+ibmId);
		return target;
	}

	public WebTarget getIBMResultDownloadUrlPath(String hubName, String groupName, String projectName, String ibmId, String jobId) {
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(urlIBMQuantum+"/Network/"+hubName+"/Groups/"+groupName+"/Projects/"+projectName+"/Jobs/"+jobId+"/resultDownloadUrl?access_token="+ibmId);
		return target;
	}
	
	public WebTarget getIBMResultDownloadedPath(String hubName, String groupName, String projectName, String ibmId, String jobId) {
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(urlIBMQuantum+"/Network/"+hubName+"/Groups/"+groupName+"/Projects/"+projectName+"/Jobs/"+jobId+"/resultDownloaded?access_token="+ibmId);
		return target;
	}
	
//Methods to get the Payloads for the IBM Quantum Experience API Methods

	public String getJobObjectStoragePayload(String backend, String shareLevel, String name, String tags, String experimentTag) {
		String payload;
		//Creates a default payload if parameters are left blank
		if(Objects.equals(backend, "") && Objects.equals(shareLevel, "") && Objects.equals(name, "") && Objects.equals(tags, "") && Objects.equals(experimentTag, "")) {
			payload = "{\"allowObjectStorage\":true,\"backend\":{\"name\":\"ibmq_qasm_simulator\"},\"shareLevel\":\"project\",\"name\":\"Test\",\"tags\":[\"test\"],\"experimentTag\":\"testTag\"}";
		} else {
			payload = "{\"allowObjectStorage\":true,\"backend\":{\"name\":\"" + backend + "\"},\"shareLevel\":\"" + shareLevel + "\",\"name\":\"" + name + "\",\"tags\":[\"" + tags + "\"],\"experimentTag\":\"" + experimentTag + "\"}";
		}
		return payload;
	}
	
	
	
//Methods for the IBM Quantum Experience API
	
	/**
	 * Sends a login request to the IBM API and returns the ibmId needed for further communication
	 * @param ibmId
	 * @param web
	 * @return ibmId
	 */
	public String loginIBM(String ibmId, WebResource web) {
		WebTarget target = web.getIBMTarget();
		WebTarget login = target.path("/users/loginWithToken");
		String apiToken = web.getIBMAPIToken();
					
		String response = login.request().post(Entity.entity(apiToken, "application/json"), String.class);
		ObjectMapper objectMapper = new ObjectMapper();
		JsonNode rootNode;
		try {
			rootNode = objectMapper.readTree(response);
			JsonNode idNode = rootNode.path("id");
			String id = idNode.asText();
			ibmId = id;
		} catch (JsonMappingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (JsonProcessingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return ibmId;
	}
	
	/**
	 * Gets all devices from the IBM API
	 * Returns them as as String
	 * @param ibmId
	 * @param web
	 * @return response
	 */
	public String getDevicesIBM(String ibmId, WebResource web) {
		WebTarget target = web.getIBMDevicesOfProjectInfoPath(standardHubName, standardGroupName, standardProjectName, ibmId);
		String response = target.request().get(String.class);
		return response;
	}
	
	public String getDeviceVersionIBM(String ibmId, WebResource web, String deviceName) {
		WebTarget target = web.getIBMDevicesOfProjectInfoPath(standardHubName, standardGroupName, standardProjectName, ibmId);
		String deviceVersion = "";
		//sends the request
		String response = target.request().get(String.class);
		
		ObjectMapper objectMapper = new ObjectMapper();
		JsonNode rootNode;
		try {
			rootNode = objectMapper.readTree(response);
			Iterator<JsonNode> rootElements = rootNode.elements();
			while(rootElements.hasNext()) {
				JsonNode element = rootElements.next();
				JsonNode backendElement = element.path("backend_name");
				JsonNode backendVersion = element.path("backend_version");
				
				String backendName = backendElement.asText();
				if(Objects.equals(backendName, deviceName)) {
					deviceVersion = backendVersion.asText();
					break;
				} else {
					deviceVersion = "Fehler";
				}
			}
			
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return deviceVersion;
	}
	
	/**
	 * Gets all devices from the IBM API
	 * Searches for the one best fit for the number of qbits
	 * Returns its name and version as a concatenated string
	 * @param ibmId
	 * @param web
	 * @param qbits
	 * @return backendInfo
	 */
	public String getFittingDeviceIBM(String ibmId, WebResource web, int qbits) {
		WebTarget target = web.getIBMDevicesOfProjectInfoPath(standardHubName, standardGroupName, standardProjectName, ibmId);
		String backendInfo = "";
		//sends the request
		String response = target.request().get(String.class);
		
		ObjectMapper objectMapper = new ObjectMapper();
		JsonNode rootNode;
		try {
			rootNode = objectMapper.readTree(response);
			Iterator<JsonNode> rootElements = rootNode.elements();
			while(rootElements.hasNext()) {
				JsonNode element = rootElements.next();
				JsonNode backendElement = element.path("backend_name");
				JsonNode backendVersion = element.path("backend_version");
				JsonNode qbitElement = element.path("n_qubits");
				
				int qb = qbitElement.asInt();
				if(Objects.equals(qb, qbits)) {
					backendInfo = backendElement.asText() + ";";
					backendInfo = backendInfo + backendVersion.asText() + ";";
					break;
				} else if(qb > qbits) {
					backendInfo = backendElement.asText() + ";";
					backendInfo = backendInfo + backendVersion.asText() + ";";
				} else {
					backendInfo = "Fehler;Fehler;";
				}
			}
			
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return backendInfo;
	}
	
	/**
	 * Creates the storage for a quantum circuit by sending a request to the IBM API
	 * Returns the jobId
	 * @param ibmId
	 * @param web
	 * @return jobId
	 */
	public String createJobObjectStorageIBM(String ibmId, WebResource web, String backend, String shareLevel, String name, String tags, String experimentTag) {
		WebTarget target = web.getIBMJobObjectStoragePath(standardHubName, standardGroupName, standardProjectName, ibmId);
		//Gets a standard payload
		String payload = getJobObjectStoragePayload(backend, shareLevel, name, tags, experimentTag);
		//sends the request
		String response = target.request().post(Entity.entity(payload, "application/json"), String.class);
		
		String jobId = "";
		ObjectMapper objectMapper = new ObjectMapper();
		JsonNode rootNode;
		try {
			rootNode = objectMapper.readTree(response);
			JsonNode idNode = rootNode.path("id");
			String id = idNode.asText();
			jobId = id;
		} catch (JsonMappingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (JsonProcessingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		return jobId;
	}
	
	/**
	 * Creates the UploadUrl for a quantum circuit by sending a request to the IBM API
	 * Returns the url
	 * @param ibmId
	 * @param web
	 * @param jobId
	 * @return url
	 */
	public String getJobUploadUrlIBM(String ibmId, WebResource web, String jobId) {
		WebTarget target = web.getIBMJobUploadUrlPath(standardHubName, standardGroupName, standardProjectName, jobId, ibmId);
		//sends the request
		String response = target.request().get(String.class);
		
		String uploadUrl = "";
		ObjectMapper objectMapper = new ObjectMapper();
		JsonNode rootNode;
		try {
			rootNode = objectMapper.readTree(response);
			JsonNode urlNode = rootNode.path("url");
			String url = urlNode.asText();
			uploadUrl = url;
		} catch (JsonMappingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (JsonProcessingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return uploadUrl;
	}
	
	/*
	 * Test if the jobUpload works with Curl
	 * Doesnt seem to....
	 * 
	public boolean uploadQObjectIBM(String url) {
		String command = "curl -H Content-type: application/json -k -X PUT -T " + qObjectPath + " " + url;
		boolean isAlive = false;
		try {
			Process process = Runtime.getRuntime().exec(command);
			isAlive = process.isAlive();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return isAlive;
	}
	*/
	
	/**
	 * Uploads a job to the specified url
	 * Returns a string if correctly done
	 * @param url
	 * @param job
	 * @return
	 */
	public String uploadJobIBM(String url, String job) {
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(url);
		
		target.request().put(Entity.entity(job, "application/json"));
		return "ausgeführt";
	}
	
	/**
	 * Notifies the IBM API that the by jobId specified job has been uploaded
	 * Returns the status of the job as a string
	 * @param ibmId
	 * @param web
	 * @param jobId
	 * @return status
	 */
	public String getJobDataUploadedIBM(String ibmId, WebResource web, String jobId) {
		WebTarget target = web.getIBMJobDataUploadedPath(standardHubName, standardGroupName, standardProjectName, ibmId, jobId);
		//sends the request
		String response = target.request().post(Entity.entity("", "application/json"), String.class);
		
		String status = "";
		ObjectMapper objectMapper = new ObjectMapper();
		JsonNode rootNode;
		try {
			rootNode = objectMapper.readTree(response);
			JsonNode statusNode = rootNode.path("status");
			status = statusNode.asText();
			System.out.println("Inhalt der status Node: " + status);
			
		} catch (JsonMappingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (JsonProcessingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return status;
	}
	
	/**
	 * Gets information about a job by its jobId
	 * Returns part of that information as a string
	 * @param ibmId
	 * @param web
	 * @param jobId
	 * @return info
	 */
	public String getJobByIdIBM(String ibmId, WebResource web, String jobId) {
		WebTarget target = web.getIBMJobByIdPath(standardHubName, standardGroupName, standardProjectName, ibmId, jobId);
		//sends the request
		String response = target.request().get(String.class);
		
		String info = "";
		ObjectMapper objectMapper = new ObjectMapper();
		JsonNode rootNode;
		try {
			rootNode = objectMapper.readTree(response);
			JsonNode statusNode = rootNode.path("status");
			String status = statusNode.asText();
			info = status;
		} catch (JsonMappingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (JsonProcessingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return info;
	}
	
	/**
	 * Creates the downloadUrl for the result of a job specified by its Id
	 * Returns the url as a String
	 * @param ibmId
	 * @param web
	 * @param jobId
	 * @return downloadUrl
	 */
	public String getResultDownloadUrlIBM(String ibmId, WebResource web, String jobId) {
		WebTarget target = web.getIBMResultDownloadUrlPath(standardHubName, standardGroupName, standardProjectName, ibmId, jobId);
		//sends the request
		String response = target.request().get(String.class);
		
		String downloadUrl = "";
		ObjectMapper objectMapper = new ObjectMapper();
		JsonNode rootNode;
		try {
			rootNode = objectMapper.readTree(response);
			JsonNode urlNode = rootNode.path("url");
			String url = urlNode.asText();
			downloadUrl = url;
		} catch (JsonMappingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (JsonProcessingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return downloadUrl;		
	}
	
	/**
	 * Downloads a job from the specified url
	 * Formats the result to only show the counts of measured states
	 * Returns the results of the job as an ArrayList<String> with the following format:  [element1;value1, element2;value2, ...]
	 * @param url
	 * @return
	 */
	public ArrayList<String> downloadJobIBM(String url) {
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(url);		
		//sends the request
		String response = target.request().get(String.class);
		
		ArrayList<String> results = new ArrayList<String>();
		String measured = "";
		ObjectMapper objectMapper = new ObjectMapper();
		JsonNode rootNode;
		try {
			rootNode = objectMapper.readTree(response);
			JsonNode resultsNode = rootNode.path("results");
			if(resultsNode.isArray()) {
				for (final JsonNode objNode : resultsNode) {
			        JsonNode countsNode = objNode.path("data").path("counts");
			        Iterator<Entry<String, JsonNode>> countsEntries = countsNode.fields();
			        
					while(countsEntries.hasNext()) {
						Entry<String, JsonNode> entry = countsEntries.next();
						measured = entry.getKey()+";";
						JsonNode element = entry.getValue();
						measured += element.asText();
						results.add(measured);
					}
			    }
			}
			
			
		} catch (JsonMappingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (JsonProcessingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		return results;
	}
	
	/**
	 * Notifies the IBM API that the by jobId specified job has been downloaded
	 * Returns the status of the job as a string
	 * @param ibmId
	 * @param web
	 * @param jobId
	 * @return status
	 */
	public String getResultDownloadedIBM(String ibmId, WebResource web, String jobId) {
		WebTarget target = web.getIBMResultDownloadedPath(standardHubName, standardGroupName, standardProjectName, ibmId, jobId);
		//sends the request
		String response = target.request().post(Entity.entity("", "application/json"), String.class);
		
		String status = "";
		ObjectMapper objectMapper = new ObjectMapper();
		JsonNode rootNode;
		try {
			rootNode = objectMapper.readTree(response);
			JsonNode terminatedNode = rootNode.path("terminated");
			String terminated = terminatedNode.asText();
			status = terminated;
		} catch (JsonMappingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (JsonProcessingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return status;
	}
	
}
