package org.unistuttgart.acaliqu;

public class QCElement {
	//Class variables
	int Id;
	String Name;
	int Qbits;
	String Date;
	int Pow;
	double[][] CalibrationMatrix;
	//Options Variables
	boolean Update;
	int UpdateInterval;
	boolean Unfolded;
	String UnfoldingMethod;
	boolean Calibrated;
	//Other variables
	boolean Official;
	int maxNumberOfShots;
	String[] CircuitsIds;
	
	//Constructor for dummy objects
	QCElement(int ID){
		this.Id = ID;
	}
	
	//Constructor without matrix
	QCElement(int ID, String GivenName, int qb, int shots){
		Id = ID;
		Name = GivenName;
		maxNumberOfShots = shots;		
	}
	
	//Empty Constructor
	QCElement(){
		
	}
	
	//Easy Constructor
	QCElement(int ID, String GivenName, int qb, String date){
		Id = ID;
		Name = GivenName;
		Date = date;
		Qbits = qb;
		Pow = (int)Math.pow(2, qb);
		CalibrationMatrix = new double[Pow][Pow];
		Update = false;
		UpdateInterval = 0;
		Unfolded = false;
		UnfoldingMethod = "MatrixInversion";
		Calibrated = false;
		Official = false;
		maxNumberOfShots = 2048;
		CircuitsIds = new String[Pow];
	}
	
	//Full Constructor for save file loading
	QCElement(int ID, String GivenName, int qb, String date, boolean update, int interval, boolean unfolded, String method, boolean calibrated, boolean official, int shots){
		Id = ID;
		Name = GivenName;
		Date = date;
		Qbits = qb;
		Pow = (int)Math.pow(2, qb);
		CalibrationMatrix = new double[Pow][Pow];
		Update = update;
		UpdateInterval = interval;
		Unfolded = unfolded;
		UnfoldingMethod = method;
		Calibrated = calibrated;
		Official = official;
		maxNumberOfShots = shots;
		CircuitsIds = new String[Pow];
	}
	
	//Getter and Setter methods
	public int getId() {
		return Id;
	}
	
	public void setId(int id) {
		Id = id;
	}
	
	public String getName() {
		return Name;
	}
	
	public void setName(String name) {
		Name = name;
	}
	
	public int getQbits() {
		return Qbits;
	}
	
	public void setQbits(int qbits) {
		Qbits = qbits;
	}
	
	public String getDate() {
		return Date;
	}

	public void setDate(String date) {
		Date = date;
	}

	public int getPow() {
		return Pow;
	}

	public void setPow(int pow) {
		this.Pow = pow;
	}

	public double[][] getCalibrationMatrix() {
		return CalibrationMatrix;
	}
	
	public void setCalibrationMatrix(double[][] calibrationMatrix) {
		CalibrationMatrix = calibrationMatrix;
	}
	
	public boolean isUpdate() {
		return Update;
	}
	
	public void setUpdate(boolean update) {
		Update = update;
	}
	
	public int getUpdateInterval() {
		return UpdateInterval;
	}
	
	public void setUpdateInterval(int updateInterval) {
		UpdateInterval = updateInterval;
	}
	
	public boolean isUnfolded() {
		return Unfolded;
	}

	public void setUnfolded(boolean unfolded) {
		Unfolded = unfolded;
	}

	public String getUnfoldingMethod() {
		return UnfoldingMethod;
	}

	public void setUnfoldingMethod(String unfoldingMethod) {
		UnfoldingMethod = unfoldingMethod;
	}

	//Get the Element from the calibrationMatrix indicated by the indices i and j
	public double getCalibrationElement(int i, int j) {
		return CalibrationMatrix[i][j];
	}
	
	public void setCalibrationElement(int i, int j, double value) {
		CalibrationMatrix[i][j] = value;
	}
	
	public boolean isCalibrated() {
		return Calibrated;
	}

	public void setCalibrated(boolean calibrated) {
		Calibrated = calibrated;
	}

	public boolean isOfficial() {
		return Official;
	}

	public void setOfficial(boolean official) {
		Official = official;
	}
	
	public int getMaxNumberOfShots() {
		return maxNumberOfShots;
	}

	public void setMaxNumberOfShots(int maxNumberOfShots) {
		this.maxNumberOfShots = maxNumberOfShots;
	}
	
	public String[] getCircuitsIds() {
		return CircuitsIds;
	}

	public void setCircuitsIds(String[] circuitsIds) {
		CircuitsIds = circuitsIds;
	}

	public String getCircuitsIdsElement(int index) {
		return CircuitsIds[index];
	}
	
	public void setCircuitsIdsElement(int index, String element) {
		CircuitsIds[index] = element;
	}
	
	public void initializeCircuitsIds() {
		for(int i = 0; i < Pow; i++) {
			CircuitsIds[i] = "-1";
		}
	}
	
	//Fill the Matrix with a specific value
	public void fillCalibrationMatrix(double value) {
		for(int i = 0; i < Pow; i++) {
			for(int j = 0; j < Pow; j++) {
				CalibrationMatrix[i][j] = value;
			}
		}
	}
	
	@Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        QCElement other = (QCElement) obj;
        if (Id != other.Id)
            return false;
        return true;
    }   	
}
