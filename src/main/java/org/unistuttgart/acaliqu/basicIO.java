package org.unistuttgart.acaliqu;
import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class basicIO {
	
	 BufferedReader inputStream = null;
	 BufferedWriter outputStream = null;
		
	public void readData() throws IOException {
		try {
			inputStream = new BufferedReader(new FileReader("save.txt"));
			
		} catch (Exception e) {
			System.out.println(e);
		} finally {
			try {
				if(inputStream != null) {
					inputStream.close();
				}				
			} catch (Exception e) {
				System.out.println(e);
			}
		}
	}
	
	public void writeData() throws IOException {
		try {
			outputStream = new BufferedWriter(new FileWriter("save.txt"));
			
		} catch (Exception e) {
			System.out.println(e);
		} finally {
			try {
				if(outputStream != null) {
					outputStream.close();
				}				
			} catch (Exception e) {
				System.out.println(e);
			}
		}
	}
	
	/*
	 * opens the InputStream if possible or throws an exception
	 */
	public BufferedReader openInputStream() throws IOException {
		try {
			inputStream = new BufferedReader(new FileReader("save.txt"));
		} catch (Exception e) {
			System.out.println(e);
		}
		return inputStream;
	}
	
	/*
	 * opens the OutputStream if possible or throws an exception
	 */
	public BufferedWriter openOutputStream() throws IOException {
		try {
			outputStream = new BufferedWriter(new FileWriter("save.txt"));
		} catch (Exception e) {
			System.out.println(e);
		}
		return outputStream;
	}
	
	/*
	 * closes the InputStream if possible or throws an exception
	 */
	public void closeInputStream(BufferedReader inputStream) throws IOException {
		try {
			if(inputStream != null) {
				inputStream.close();
			}
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	/*
	 * closes the OutputStream if possible or throws an exception
	 */
	public void closeOutputStream(BufferedWriter outputStream) throws IOException {
		try {
			if(outputStream != null) {
				outputStream.close();
			}
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	/**
	 * writes the start of the save file (Header)
	 * @param outputStream
	 * @param list
	 * @throws IOException
	 */
	public void writeStart(BufferedWriter outputStream, QCList list, String userToken) throws IOException {
		int number = list.size();
		try {
			outputStream.write(Integer.toString(number));
			outputStream.newLine();
			outputStream.write(userToken);
			outputStream.newLine();
			outputStream.write("####");
			outputStream.newLine();
			outputStream.flush();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	/**
	 * writes the end char of the save file
	 * @param outputStream
	 * @param list
	 * @throws IOException
	 */
	public void writeEnd(BufferedWriter outputStream, QCList list) throws IOException {
		try {
			outputStream.write("%");
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	/**
	 * Method to save a single QC Object by writing its contents to the save file
	 * @param outputStream
	 * @param list
	 * @param index
	 * @throws IOException
	 */
	public void saveQC(BufferedWriter outputStream, QCList list, int index) throws IOException {
		// gets all the elements of the QC specified by index
		int id = list.getQC(index).getId();
		String name = list.getQC(index).getName();
		int qb = list.getQC(index).getQbits();
		String date = list.getQC(index).getDate();
		boolean update = list.getQC(index).isUpdate();
		int interval = list.getQC(index).getUpdateInterval();
		boolean unfolding = list.getQC(index).isUnfolded();
		String unfoldingMethod = list.getQC(index).getUnfoldingMethod();
		boolean calibrated = list.getQC(index).isCalibrated();
		boolean official = list.getQC(index).isOfficial();
		int shots = list.getQC(index).getMaxNumberOfShots();
		
		double pow = Math.pow(2, qb);
		// tries to write all these Elements into the file
		try {
			//writes all aditional information into the buffer
			outputStream.write(Integer.toString(id));
			outputStream.newLine();
			
			outputStream.write(name);
			outputStream.newLine();
			
			outputStream.write(Integer.toString(qb));
			outputStream.newLine();
			
			outputStream.write(date);
			outputStream.newLine();
			
			outputStream.write(Boolean.toString(update));
			outputStream.newLine();
			
			outputStream.write(Integer.toString(interval));
			outputStream.newLine();
			
			outputStream.write(Boolean.toString(unfolding));
			outputStream.newLine();
			
			outputStream.write(unfoldingMethod);
			outputStream.newLine();
			
			outputStream.write(Boolean.toString(calibrated));
			outputStream.newLine();
			
			outputStream.write(Boolean.toString(official));
			outputStream.newLine();
			
			outputStream.write(Integer.toString(shots));
			outputStream.newLine();
			
			String line = "";
			//writes the CircuitsIds as a line into the buffer
			for(int i = 0; i < pow; i++) {
				line += list.getQC(index).getCircuitsIdsElement(i) + ";"; 
			}
			outputStream.write(line);
			outputStream.newLine();
			
			//writes the calibration matrix line by line into the buffer
			for(int i = 0; i < pow; i++) {
				line = list.getMatrixLine(index, i);
				outputStream.write(line);
				outputStream.newLine();
			}
			outputStream.write("####");
			outputStream.newLine();
			outputStream.flush();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	public String readStart(BufferedReader inputStream) throws IOException {
		String line = "";
		String output = "";
		try {
			line = inputStream.readLine();
			output = line + ";";
			line = inputStream.readLine();
			output += line + ";";
			
		} catch (Exception e) {
			System.out.println(e);
		}
		return output;
	}
	
	public void readEnd(BufferedReader inputStream) throws IOException {
		
	}
	
	public void loadQC(BufferedReader inputStream, QCList list, int qcs, int index) throws IOException {
		String line = "";
		float element = 0;
		String[] nmbrs;
		try {
			//first line is the separator
			line = inputStream.readLine();
			//System.out.println(line);
			//ID
			line = inputStream.readLine();
			int id = Integer.parseInt(line);
			//Name
			String name = inputStream.readLine();
			//number of qbits
			line = inputStream.readLine();
			int qb = Integer.parseInt(line);
			//System.out.println("number qbs: " + qb);
			//date
			String date = inputStream.readLine();
			//automatic update
			line = inputStream.readLine();
			boolean update = Boolean.parseBoolean(line);
			//System.out.println("update: " + line);
			//update interval
			line = inputStream.readLine();
			int interval = Integer.parseInt(line);
			//System.out.println("interval: " + interval);
			//unfolding status
			line = inputStream.readLine();
			boolean unfolding = Boolean.parseBoolean(line);
			//unfoldingMethod
			String unfoldingMethod = inputStream.readLine();
			//System.out.println("unfolding method: " + unfolding);
			//Calibration Status
			line = inputStream.readLine();
			boolean calibrated = Boolean.parseBoolean(line);
			//Official status
			line = inputStream.readLine();
			boolean official = Boolean.parseBoolean(line);
			//Max Number of shots
			line = inputStream.readLine();
			int shots = Integer.parseInt(line);
			//create QCElement and add it to the list
			QCElement qc = new QCElement(id, name, qb, date, update, interval, unfolding, unfoldingMethod, calibrated, official, shots);
			list.addQCElement(qc);
			//circuitsIds
			line = inputStream.readLine();
			String[] elements = line.split(";");
			for(int i = 0; i < elements.length; i++) {
				qc.setCircuitsIdsElement(i, elements[i]);
			}
			//calibrationMatrix
			double pow = Math.pow(2, qb);
			for(int i = 0; i < pow; i++) {
				nmbrs = inputStream.readLine().split("\\s");
				for(int j = 0; j < qb; j++) {
					element = Float.parseFloat(nmbrs[j]);
					//System.out.println("I: "+ i+" J: "+ j+" el: "+ element);
					qc.setCalibrationElement(i, j, element);
					
				}
			}
		} catch (Exception e) {
			System.out.println(e);
		}		
	}

}
/*
 * The Format of the save file "save.txt" is as follows:
 * 
 * - number of QCs saved in this file (e.g. 2)
 * - the userToken needed to communicate with the IBM Quantum Experience API
 * - Separator "####"
 * All QCs one after the other separated by #
 * - int					ID of the first QC (e.g 1)
 * - String					Name (e.g QC 1)
 * - int					Number of Qbits (e.g 2)
 * - String/LocalDateTime	Date and Time of Creation/last update
 * - String/bool			Automatic update of the matrix (true or false)
 * - int					The update interval (e.g 5)
 * - String/bool			The Unfolding status of the CalibrationMatrix (true if already unfolded and false otherwise)
 * - String					The Unfolding Method used for this CalibrationMatrix
 * - String/bool			The Calibration Status
 * - String/bool			The Official Status (is it one of the Quantum Computers provided by IBM)
 * - int					Max number of shots
 * - String[]				the circuits Ids needed for calibration in a single line with ";" as a separator
 * - float					the calibration Matrix line by line e.g.:
 * - 0,5;
 * - 2,6;
 * - Separator "####"
 * Next QC
 * ...
 * - End of file "%%"
 */