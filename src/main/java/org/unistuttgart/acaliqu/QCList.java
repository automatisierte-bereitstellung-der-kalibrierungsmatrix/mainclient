package org.unistuttgart.acaliqu;
import java.util.ArrayList;
import java.util.List;

public class QCList {
	private static QCList instance;
	private static ArrayList <QCElement> qcList = new ArrayList<QCElement>();
	
	QCList() {
		
	}
	
	/**
	 * returns the instance of the QCList
	 * @return instance
	 */
	public static QCList getInstance() {
		if (instance == null) {
			instance = new QCList();
		}
		return instance;
	}
	
	/**
	 * lists all Contents of the QCList in an ArrayList
	 * @return ArrayList
	 */
	public List<QCElement> listAll(){
		return new ArrayList<QCElement>(qcList);
	}
	
	/**
	 * adds an Object of the type QCElement to the QCList and returns its id
	 * @param element
	 * @return id
	 */
	public int addQCElement(QCElement element) {
		qcList.add(element);
		return element.getId();
	}
	
	/**
	 * gets a QCElement Object with the desired id
	 * @param id
	 * @return QCElement
	 */
	public QCElement getQCElement(int id) {
		QCElement searchElement = new QCElement(id);
		int index = qcList.indexOf(searchElement);
		if(index >= 0) {
			return qcList.get(index);
		}
		return null;
	}
	
	/**
	 * deletes a QCElement Object from the QCList with the desired id and returns true if it succeeds
	 * @param id
	 * @return boolean
	 */
	public boolean deleteQCElement(int id) {
		QCElement searchElement = new QCElement(id);
		int index = qcList.indexOf(searchElement);
		if(index >= 0) {
			qcList.remove(index);
			return true;
		}
		return false;
	}
	
	/**
	 * updates a QCElement in the QCList and returns true if it succeeds
	 * @param element
	 * @return boolean
	 */
	public boolean updateQCElement(QCElement element) {
		int index = qcList.indexOf(element);
		if(index >= 0) {
			qcList.set(index, element);
			return true;
		}
		return false;
	}
	
	
	/**
	 * returns the number of Elements in this list
	 * @return size
	 */
	public int size() {
		return qcList.size();
	}
	
	/**
	 * returns the QC Element at the position indicated by index
	 * @param index
	 * @return QCElement
	 */
	public QCElement getQC(int index) {
		return qcList.get(index);
	}
	
	/**
	 * gets the index of an Element specified by its id in the QCList
	 * @param id
	 * @return index/position
	 */
	public int getQCPosition(int id) {
		int pos = 0;
		for(int i = 0; i < qcList.size(); i++) {
			if (qcList.get(i).getId() == id) {
				pos = i;
				break;
			}
		}
		return pos;
	}
	
	/*
	 * returns the Value of the Element of the Calibration Matrix of the QC Element at the position indicated by i and j
	 */
	public double getMatrixElement(int index, int i, int j) {
		return qcList.get(index).getCalibrationElement(i, j);
	}
	
	/*
	 * fills the matrix of the QC Elemet specified by index with a specific value
	 */
	public void fillMatrix(int index, double value) {
		qcList.get(index).fillCalibrationMatrix(value);
	}
	
	/**
	 * writes a single line of the matrix specified by line and index into the String output
	 * @param index
	 * @param line
	 * @return output
	 */
	public String getMatrixLine(int index, int line) {
		String output = "";
		if(line >= 0 && line < qcList.get(index).getPow()) {
			for(int j = 0; j < qcList.get(index).getPow(); j++) {
				output += qcList.get(index).getCalibrationElement(line, j) + " ";
			}
			return output;
		} else {
			output = "Fehler! Falsche Zeile";
			return output;
		}
	}
	
	/**
	 * writes the Matrix into the String[] output line by line
	 * @param index
	 * @return output[] 
	 */
	public String[] getMatrix(int index) {
		String[] output = new String[qcList.get(index).getPow()];
		String text;
		for(int i = 0; i < qcList.get(index).getPow(); i++) {
			text = "";
			for(int j = 0; j < qcList.get(index).getPow(); j++) {
				text += qcList.get(index).getCalibrationElement(i, j) + " ";
			}
			output[i] = text;
		}
		return output;
	}	
}
