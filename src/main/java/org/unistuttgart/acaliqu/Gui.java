package org.unistuttgart.acaliqu;
import javax.swing.*;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;
import org.unistuttgart.quantumcircuit.QuantumCircuit;
import org.unistuttgart.unfolding.Unfolding;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Objects;
import org.unistuttgart.quantumcircuit.*;

public class Gui extends JFrame implements ActionListener {
	//Class variables
	JFrame myFrame;
	JPanel panel0;
	JButton createQC;
	JButton save;
	JButton load;
	JButton saveServer;
	JButton loadServer;
	JButton test;
	JButton testServer;
	JButton loginIBM;
	JButton createIBMQC;
	
	String ibmId;
	String userToken = "default";
	
	//static stuff for getting the correct id
	static int numberofPanels = 0;
	static int[] panelpos = new int[50];
	
	
	//QCList for the gui
	private final QCList qcList;
	
	//IO for the gui
	private final basicIO IO;
	
	//Constructor
	public Gui(QCList list, basicIO io) {
		this.qcList = list;
		this.IO = io;
		
		//sets all Elements of the positioning array to -1
		initializePanelPos();
		/*
		 * erstellt einen Frame (Hintergrund) mit einem Namen, einer bestimmten Gr��e
		 * und einem Layout
		 */
		myFrame = new JFrame ("ACaliQu");
		myFrame.setSize(500, 500);
		myFrame.setLayout(new java.awt.GridLayout());
		
		/*
		 * Darauf k�nnen nun sogenannte Panels angebracht werden, die wiederum eigene Layouts besitzen k�nnen
		 */
		panel0 = new JPanel();
		panel0.setLayout(new java.awt.FlowLayout());
		
		/*
		 * Verschiedene Buttons werden erstellt
		 */
		createQC = new JButton();
		createQC.setText("Neuen QC erstellen");
		createQC.addActionListener(this);
		
		save = new JButton();
		save.setText("in die Datei speichern");
		save.addActionListener(this);
		
		load = new JButton();
		load.setText("von der Datei laden");
		load.addActionListener(this);
		
		saveServer = new JButton();
		saveServer.setText("auf dem Server speichern");
		saveServer.addActionListener(this);
		
		loadServer = new JButton();
		loadServer.setText("vom Server laden");
		loadServer.addActionListener(this);
		
		loginIBM = new JButton();
		loginIBM.setText("IBM login");
		loginIBM.addActionListener(this);
		
		createIBMQC = new JButton();
		createIBMQC.setText("IBM QCs erstellen");
		createIBMQC.addActionListener(this);
		
		/*
		 * Buttons werden den Panels hinzugef�gt
		 */
		panel0.add(createQC);
		panel0.add(save);
		panel0.add(load);
		panel0.add(saveServer);
		panel0.add(loadServer);
		panel0.add(loginIBM);
		panel0.add(createIBMQC);
		
		/*
		 * Panel wird dem Frame hinzugef�gt
		 */
		myFrame.add(panel0);
		
		/*
		 * Exit on close
		 */
		myFrame.setDefaultCloseOperation(EXIT_ON_CLOSE);
		myFrame.setVisible(true);
		
	}
	
	/*
	 * functionality of the buttons
	 */
	public void actionPerformed(ActionEvent e) {
		//Falls der CreateQC Button gedr�ckt wurde
		if(e.getSource() == createQC) {
			//gets the user input for the creation of the QC
			String information= getUserInput();
			String[] parts = information.split(";");
			String name = parts[0];
			String qbits = parts[1];
			
			int qbit = Integer.parseInt(qbits);
			int id = nextPanelPos();
			LocalDateTime unformattedDate = LocalDateTime.now();
	        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
	        String date = unformattedDate.format(formatter);
			createNewQC(myFrame, id, name, qbit, unformattedDate);
			QCElement qc = new QCElement(id, name, qbit, date);
			qc.fillCalibrationMatrix(0);
			qc.initializeCircuitsIds();
			qcList.addQCElement(qc);
		}
		
		//Falls der Save Button gedr�ckt wurde
		else if(e.getSource() == save) {
			try {
				BufferedWriter outputStream = IO.openOutputStream();
				IO.writeStart(outputStream, qcList, userToken);
				for(int i = 0; i < qcList.size(); i++) {
					IO.saveQC(outputStream, qcList, i);
				}
				IO.writeEnd(outputStream, qcList);
				IO.closeOutputStream(outputStream);
			} catch (IOException e1) {
				e1.printStackTrace();
				System.out.println(e1);
			}
			
		}
		
		//Falls der Load Button gedr�ckt wurde
		else if(e.getSource() == load) {
			//TODO vor dem Laden alle aktiven Panels l�schen, sodass die GUI wieder im Originalzustand ist
			
			try {
				BufferedReader inputStream = IO.openInputStream();
				String information = IO.readStart(inputStream);
				String[] parts = information.split(";");
				int qcs = Integer.parseInt(parts[0]);
				userToken = parts[1];
				//System.out.println(qcs);
				for(int i = 0; i < qcs; i++) {
					//loads contents of a single QC and adds them to the list
					IO.loadQC(inputStream, qcList, qcs, i);
					//gets variables from the added QC needed for new panel creation
					int id = qcList.getQC(i).getId();
					String name = qcList.getQC(i).getName();
					int qbits = qcList.getQC(i).getQbits();
					String unformattedDate = qcList.getQC(i).getDate();
					DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
					LocalDateTime date = LocalDateTime.parse(unformattedDate, formatter);
					createNewQC(myFrame, id, name, qbits, date);
				}
				IO.readEnd(inputStream);
				IO.closeInputStream(inputStream);
			} catch (Exception e2) {
				e2.printStackTrace();
				System.out.println(e2);
			}
		}
		
		//Falls der saveServer Button gedr�ckt wurde
		else if(e.getSource() == saveServer) {
			WebResource web = new WebResource();
			WebTarget target = web.getServerTarget();
			WebTarget resourceTarget = target.path("/ids");
			for(int i = 0; i < qcList.size(); i++) {
				Response response = resourceTarget.request().post(Entity.entity(qcList.getQC(i), "application/json"), Response.class);
				System.out.println(response.getLocation().toString());
			}
		}
		
		//Falls der loadServer Button gedr�ckt wurde
		else if(e.getSource() == loadServer) {
			// TODO  vor dem Laden alle aktiven Panels l�schen, sodass die GUI wieder im Originalzustand ist
			WebResource web = new WebResource();
			WebTarget target = web.getServerTarget();
			WebTarget getIdsTarget = target.path("/list/ids");
			
			String qcids = getIdsTarget.request().get(String.class);
			String[] parts = qcids.split(";");
			int countQCs = Integer.parseInt(parts[0]);
			if(countQCs > 0) {
				for(int i = 1; i <= countQCs; i++) {
					WebTarget getTarget = target.path("/ids/" + parts[i]);
					QCElement response = getTarget.request().accept("application/json").get(QCElement.class);
					qcList.addQCElement(response);
					int id = response.getId();
					String name = response.getName();
					int qbits = response.getQbits();
					String unformattedDate = response.getDate();
					DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
					LocalDateTime date = LocalDateTime.parse(unformattedDate, formatter);
					createNewQC(myFrame, id, name, qbits, date);
				}				
			}
			
		}
		
		//Falls der loginIBM Button gedr�ckt wurde
		else if(e.getSource() == loginIBM) {
			WebResource web = new WebResource();
			
			if(Objects.equals(userToken, "default")) {
				int check = 0;
								
				JTextField tokenField = new JTextField();
				final JComponent[] inputs = new JComponent[] {
						new JLabel("IBM Quantum Experience API Token"),
						tokenField,
				};
				while(check == 0) {
					int result = JOptionPane.showConfirmDialog(null, inputs, "Bitte geben Sie das Token ein", JOptionPane.PLAIN_MESSAGE);

					if (result == JOptionPane.OK_OPTION) {
						//Wenn das Feld freigelassen wurde
						if(Objects.equals(tokenField.getText(), "")) {
							JOptionPane.showMessageDialog(null,
								    "Bitte das Feld ausf�llen.",
								    "Fehler",
								    JOptionPane.ERROR_MESSAGE);
						} else {
							//Falls das Token eingegeben wurde, wird es gespeichert
							userToken = tokenField.getText();
							
							System.out.println(userToken);
							check = 1;
						}					
					}						
				}
				web.setUserToken(userToken);
				ibmId = web.loginIBM(ibmId, web);
				
			} else {
				ibmId = web.loginIBM(ibmId, web);
			}		
		}		
		
		//Falls der createIBMQC Button gedr�ckt wurde
		else if(e.getSource() == createIBMQC) {
			if(Objects.equals(ibmId, null)) {
				JOptionPane.showMessageDialog(null,
					    "Bitte zuerst bei IBM einloggen.",
					    "Fehler, nicht angemeldet",
					    JOptionPane.ERROR_MESSAGE);
			} else {
				WebResource web = new WebResource();
				String response = web.getDevicesIBM(ibmId, web);
				
				ObjectMapper objectMapper = new ObjectMapper();
				JsonNode rootNode;
				try {
					rootNode = objectMapper.readTree(response);
					Iterator<JsonNode> rootElements = rootNode.elements();
					while(rootElements.hasNext()) {
						JsonNode element = rootElements.next();
						JsonNode backendElement = element.path("backend_name");
						JsonNode qbitElement = element.path("n_qubits");
						JsonNode shotsElement = element.path("max_shots");
						
						String name = backendElement.asText();
						int qb = qbitElement.asInt();
						LocalDateTime unformattedDate= LocalDateTime.now();
						DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
						String date = unformattedDate.format(formatter);
						int shots = shotsElement.asInt();
						//Excludes the IBM qasm simulator and ibm melbourne because 32 and 15 qbits are too much
						if(Objects.equals(name, "ibmq_qasm_simulator") || Objects.equals(name, "ibmq_16_melbourne")) {
							//Do not create a QCElement
						} else {
							int id = nextPanelPos();
							QCElement qc = new QCElement(id, name, qb, date, false, 0, false, "MatrixInversion", false, true, shots);
							qcList.addQCElement(qc);
							qc.initializeCircuitsIds();
							createNewQC(myFrame, id, name, qb, unformattedDate);
						}				
												
					}
				} catch (JsonProcessingException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * Method to create a new QC Object via a JPanel and add it to the frame
	 * @param frame, i
	 * @return frame
	 */
	public javax.swing.JFrame createNewQC(javax.swing.JFrame frame, int id, String name, int qbits, LocalDateTime date){
		JPanel panel = new JPanel();
		//TODO Grid Bag Layout statt Flow Layout um die Texte etc anzupassen
		panel.setLayout(new java.awt.FlowLayout());
		
		JLabel text1 = new JLabel("Quantencomputer: ");
		JLabel labelId = new JLabel(Integer.toString(id));
		JLabel text2 = new JLabel("Name: ");
		JLabel labelName = new JLabel(name);
		JLabel text3 = new JLabel("Anzahl Qbits: ");
		JLabel labelQbits = new JLabel(Integer.toString(qbits));
		JLabel text5 = new JLabel("Datum: ");
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		JLabel labelDate = new JLabel(dtf.format(date));
		
		//TODO Funktionalit�t der Buttons einbauen
		JButton abrufen = new JButton();
		abrufen.setText("Abrufen");
		abrufen.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				//takes the id of the panel (panel and QCElement have the same id)
				int id = Integer.parseInt(labelId.getText());
				showCalibrationMatrix(id);
			}
			
		});
		
		JButton senden = new JButton();
		senden.setText("Senden");
		senden.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				/*
				 * Falls man eingeloggt ist, werden alle ben�tigten Kalibrierungsschaltkreise erstellt,
				 * an die IBM API gesendet und die JobIds der Schaltkreise gespeichert 
				 */
				WebResource web = new WebResource();
				QuantumCircuit quCi = new QuantumCircuit();
				ObjectMapper mapper = new ObjectMapper();
				int id = Integer.parseInt(labelId.getText());
				int index = qcList.getQCPosition(id);
				int qbits = Integer.parseInt(labelQbits.getText());
				double pow = Math.pow(2, qbits);
				String job ="";
				
				if(Objects.equals(ibmId, null)) {
					JOptionPane.showMessageDialog(null,
						    "Bitte zuerst bei IBM einloggen.",
						    "Fehler, nicht angemeldet",
						    JOptionPane.ERROR_MESSAGE);
				} else {
					
					if(qcList.getQC(index).isOfficial()) {
						//Bei offiziellen QCs kann der Circuit sofort erstellt werden
						String backendName = qcList.getQC(index).getName();
						String backendVersion = web.getDeviceVersionIBM(ibmId, web, backendName);
						//Iterates over the Elements of CircuitsIds Array to create and send all Circuits and save their Ids in the Array
						for(int i = 0; i < pow; i++) {
							quCi = quCi.createCalibrationCircuit(quCi, backendName, backendVersion, qbits, 1024, i);
							try {
								//Use only to control structure of the qObject
								//	mapper.writeValue(new File("qObject.json"), quCi);
								job = mapper.writeValueAsString(quCi);
							} catch (JsonGenerationException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							} catch (JsonMappingException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							} catch (IOException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							String experimentName = backendName + " Kalibrierung Schritt " + i;
							String jobId = web.createJobObjectStorageIBM(ibmId, web, backendName, "project", experimentName, "calibration", "calibration");
							qcList.getQC(index).CircuitsIds[i] = jobId;
							String jobUrl = web.getJobUploadUrlIBM(ibmId, web, jobId);
							String confirm = web.uploadJobIBM(jobUrl, job);
							String status = web.getJobDataUploadedIBM(ibmId, web, jobId);
							System.out.println("confirm: "+ confirm+ " status: "+ status);
						}
						
					} else {	
						
						String backendInfo = web.getFittingDeviceIBM(ibmId, web, qbits);
						String[] info = backendInfo.split(";");
						String backendName = info[0];
						String backendVersion = info[1];
						//Iterate over the Elements of CircuitsIds Array to create and send all Circuits and save their Ids in the Array
						for(int i = 0; i < pow; i++) {
							quCi = quCi.createCalibrationCircuit(quCi, backendName, backendVersion, qbits, 1024, i);
							try {
								//Use only to control structure of the qObject
								//	mapper.writeValue(new File("qObject.json"), quCi);
								job = mapper.writeValueAsString(quCi);
							} catch (JsonGenerationException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							} catch (JsonMappingException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							} catch (IOException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							String experimentName = backendName + " Kalibrierung Schritt " + i;
							String jobId = web.createJobObjectStorageIBM(ibmId, web, backendName, "project", experimentName, "calibration", "calibration");
							qcList.getQC(index).setCircuitsIdsElement(i, jobId);
							String jobUrl = web.getJobUploadUrlIBM(ibmId, web, jobId);
							String confirm = web.uploadJobIBM(jobUrl, job);
							String status = web.getJobDataUploadedIBM(ibmId, web, jobId);
							System.out.println("confirm: "+ confirm+ " status: "+ status);
						}
					}				
				}
			}	
		});
		
		JButton empfangen = new JButton();
		empfangen.setText("Empfangen");
		empfangen.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				/*
				 * Falls man eingeloggt ist, wird der Status der Jobs f�r die Schaltkreise �berpr�ft
				 * und falls diese fertig sind, die Ergebnisse in die Kalibrierungsmatrix eingetragen
				 */
				WebResource web = new WebResource();
				ObjectMapper mapper = new ObjectMapper();
				int id = Integer.parseInt(labelId.getText());
				int index = qcList.getQCPosition(id);
				int qbits = Integer.parseInt(labelQbits.getText());
				double pow = Math.pow(2, qbits);
				String jobId;
				
				if(Objects.equals(ibmId, null)) {
					JOptionPane.showMessageDialog(null,
						    "Bitte zuerst bei IBM einloggen.",
						    "Fehler, nicht angemeldet",
						    JOptionPane.ERROR_MESSAGE);
				} else {
					//Iterates over the Calibration Matrix Column by Column
					int count = 0;
					for(int i = 0; i < pow; i++) {
						//sets all Elements of the Column to be calibrated to 0.0
						for(int j = 0; j < pow; j++) {
							qcList.getQC(index).setCalibrationElement(j, i, 0.0);
						}
						
						jobId = qcList.getQC(index).getCircuitsIdsElement(i);
						//Case 1: No Calibration Circuit has been sent (element is initialized with -1)
						if(Objects.equals(jobId, "-1")) {
							count ++;
						} 
						//Case 2: Already calibrated so no need to check for a result
						else if(Objects.equals(jobId, "0")) {
							// TODO DO NOTHING
						}
						//Case 3: Calibration Circuit has been sent but no result received
						else {
							//check if the result is available
							String status = web.getJobByIdIBM(ibmId, web, jobId);
							if(Objects.equals(status, "Successful completion")) {
								//download the result and notify the API of the download
								String url = web.getResultDownloadUrlIBM(ibmId, web, jobId);
								ArrayList<String> result = web.downloadJobIBM(url);
								status = web.getResultDownloadedIBM(ibmId, web, jobId);
								int key;
								double value;
								//i ist die Spalte der Kalibrierungsmatrix, k iteriert �ber die Ergebnisse der Messung
								for(int k = 0; k < result.size(); k++) {
									//Ein Ergebnis besteht aus 2 Teilen, dem Key(der hex Zahl des Zustandes) und dem Wert
									String[] parts = result.get(k).split(";");
									//Key ist damit auch die Zeile des Ergebnisses und wird hier in einen integer geparsed
									key = Integer.parseInt(parts[0], 16);
									value = Double.parseDouble(parts[2]);
									qcList.getQC(index).setCalibrationElement(key, i, value);
								}
								
							} else {
								// TODO DO NOTHING
							}
						}
					}
					//If all Calibration results are downloaded and stored in the Matrix set the calibrated status to true
					if(Objects.equals(count, 0)) {
						qcList.getQC(index).setCalibrated(true);
					}
				}
			}
		});
		
		JButton unfolding = new JButton();
		unfolding.setText("Unfolding");
		unfolding.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Unfolding unfold = new Unfolding();
				int id = Integer.parseInt(labelId.getText());
				int index = qcList.getQCPosition(id);
				//Check if already calibrated before unfolding
				if(qcList.getQC(index).isCalibrated()) {
					double[][] matrix = qcList.getQC(index).getCalibrationMatrix();
					String unfoldingMethod = qcList.getQC(index).getUnfoldingMethod();
					int nColumns = qcList.getQC(index).getPow();
					matrix = unfold.applyUnfoldingMethod(matrix, unfoldingMethod, nColumns);
					qcList.getQC(index).setCalibrationMatrix(matrix);
					qcList.getQC(index).setUnfolded(true);
					qcList.getQC(index).setCalibrated(false);					
				} else {
					JOptionPane.showMessageDialog(null,
						    "Bitte die Matrix zuerst messen.",
						    "Fehler, Kalibrierungsmatrix nicht vollst�ndig gemessen",
						    JOptionPane.ERROR_MESSAGE);
				}
			}
			
		});
		
		
		JButton optionen = new JButton();
		optionen.setText("Optionen");
		optionen.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				int id = Integer.parseInt(labelId.getText());
				int index = qcList.getQCPosition(id);
				int check = 0;
				int error = 1;
				int number = 0;
				
				JPanel panel = new JPanel();
				panel.setLayout(new java.awt.FlowLayout());
				//Buttons and TextFields
				JLabel autoAktualisierung = new JLabel();
				autoAktualisierung.setText("Automatische Aktualisierung: ");
				
				JButton AktualisierungToggle = new JButton();
				if(qcList.getQC(index).isUpdate() == true) {
					AktualisierungToggle.setText("an");
				} else {
					AktualisierungToggle.setText("aus");
				}
				AktualisierungToggle.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						boolean toggle = !qcList.getQC(index).isUpdate();
						qcList.getQC(index).setUpdate(toggle);
						if(qcList.getQC(index).isUpdate() == true) {
							AktualisierungToggle.setText("an");
						} else {
							AktualisierungToggle.setText("aus");
						}
					}
					
				});
				
				JLabel Kalibrierungsstatus = new JLabel();
				Kalibrierungsstatus.setText("Kalibriert: " + Boolean.toString(qcList.getQC(index).isCalibrated()));
				
				JLabel AktualisierungsIntervallLabel = new JLabel();
				AktualisierungsIntervallLabel.setText("Aktueller Intervall: ");
				
				JTextField AktualisierungsIntervallText = new JTextField();
				AktualisierungsIntervallText.setText(Integer.toString(qcList.getQC(index).getUpdateInterval()));
				
				JLabel UnfoldingStatus = new JLabel();
				UnfoldingStatus.setText("Unfolding Methode angewandt: "+ Boolean.toString(qcList.getQC(index).isUnfolded()));
				
				JLabel UnfoldingMethodLabel = new JLabel();
				UnfoldingMethodLabel.setText("Aktuelle Unfolding Methode: ");
				
				JTextField UnfoldingMethodText = new JTextField();
				UnfoldingMethodText.setText(qcList.getQC(index).getUnfoldingMethod());
				
				//add all Components to panel
				panel.add(Kalibrierungsstatus);
				panel.add(autoAktualisierung);
				panel.add(AktualisierungToggle);
				panel.add(AktualisierungsIntervallLabel);
				panel.add(AktualisierungsIntervallText);
				panel.add(UnfoldingStatus);
				panel.add(UnfoldingMethodLabel);
				panel.add(UnfoldingMethodText);
				
				while(check == 0) {
					int result = JOptionPane.showConfirmDialog(null, panel, "Optionsmen� " + labelName.getText(), JOptionPane.PLAIN_MESSAGE);

					if (result == JOptionPane.OK_OPTION) {
						//Wenn mindestens 1 Feld freigelassen wurde, wird eine Fehlermeldung ausgegeben
						if(Objects.equals(AktualisierungsIntervallText.getText(), "") ) {
							JOptionPane.showMessageDialog(null,
								    "Bitte alle Felder ausf�llen.",
								    "Fehler",
								    JOptionPane.ERROR_MESSAGE);
						} else {
							//�berpr�fung ob im Intervall Feld eine positive Zahl eingegeben wurde, wenn nicht werden Fehlermeldungen ausgegeben
							try {
								number = Integer.parseInt(AktualisierungsIntervallText.getText());
								if(number < 0) {
									JOptionPane.showMessageDialog(null, 
											"Bitte eine positive Zahl eingeben.",
											"Fehler",
											JOptionPane.ERROR_MESSAGE);
								} else {
									error = 0;
								}
							} catch (Exception exception) {
								JOptionPane.showMessageDialog(null, 
										"Bitte eine Zahl eingeben",
										"Fehler",
										JOptionPane.ERROR_MESSAGE);
							}
						}
						//Falls alle Eingaben stimmen, werden die Variablen im QC Element gesetzt
						if(error == 0) {
							qcList.getQC(index).setUpdateInterval(number);
							AktualisierungsIntervallText.setText(Integer.toString(number));
							check = 1;
						}
					} else {
						
					}
				}
				
			}
			
		});
		
		JButton l�schen = new JButton();
		l�schen.setText("L�schen");
		l�schen.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				//takes the id of the panel and passes it to decreasePanelPos (panel and QCElement have the same id)
				int i = Integer.parseInt(labelId.getText());
				decreasePanelPos(i);
				
				//L�sche das QC Element aus der Liste
				qcList.deleteQCElement(i);
				
				//Entferne alle Elemente des Panels und das Panel selbst
				panel.removeAll();
				frame.remove(panel);
				frame.revalidate();
				frame.repaint();
				
			}
			
		});
		
		/*
		 * f�ge alle Labels und Buttons dem Panel hinzu
		 */
		panel.add(text1);
		panel.add(labelId);
		panel.add(text2);
		panel.add(labelName);
		panel.add(text3);
		panel.add(labelQbits);
		panel.add(text5);
		panel.add(labelDate);
		panel.add(abrufen);
		panel.add(senden);
		panel.add(empfangen);
		panel.add(unfolding);
		panel.add(optionen);
		panel.add(l�schen);
		
		frame.add(panel);
		frame.revalidate();
		
		return frame;
	}
	
	
	
	/**
	 * Method to ask the user for the name and place of the QC
	 * Uses ";" as a Separator between strings and ";;" as a line end character
	 * @return User Input as string
	 */
	public String getUserInput() {
		String data = "";
		int check = 0;
		int error = 1;
		
		JTextField name = new JTextField();
		JTextField qbits = new JTextField();
		final JComponent[] inputs = new JComponent[] {
				new JLabel("Name des QCs"),
				name,
				new JLabel("Anzahl der Qbits"),
				qbits,
		};
		while(check == 0) {
			int result = JOptionPane.showConfirmDialog(null, inputs, "Bitte geben Sie die Daten ein", JOptionPane.PLAIN_MESSAGE);

			if (result == JOptionPane.OK_OPTION) {
				//Wenn mindestens 1 Feld freigelassen wurde, wird eine Fehlermeldung ausgegeben
				if(Objects.equals(name.getText(), "") || Objects.equals(qbits.getText(), "")) {
					JOptionPane.showMessageDialog(null,
						    "Bitte alle Felder ausf�llen.",
						    "Fehler",
						    JOptionPane.ERROR_MESSAGE);
				} else {
					//�berpr�fung ob im Qbit Feld eine Zahl gr��er 0 eingegeben wurde, wenn nicht werden Fehlermeldungen ausgegeben
					try {
						int number = Integer.parseInt(qbits.getText());
						if(number <= 0 || number > 100) {
							JOptionPane.showMessageDialog(null, 
									"Bitte eine Zahl > 0 & < 100 eingeben.",
									"Fehler",
									JOptionPane.ERROR_MESSAGE);
						} else {
							error = 0;
						}
					} catch (Exception e) {
						JOptionPane.showMessageDialog(null, 
								"Bitte eine Zahl eingeben",
								"Fehler",
								JOptionPane.ERROR_MESSAGE);
					}
				}
				//Falls alle Eingaben stimmen, werden die Daten als String zur�ckgegeben
				if(error == 0) {
					// ; dient als Separator und ;; als endzeichen
					data = name.getText() + ";" + qbits.getText() + ";";
					
					System.out.println(data);
					check = 1;
				}
			} else {
				data = " ; ; ; ;;";
			}
		}
		
		return data;
	}
	
	/**
	 * Method to display the Calibration Matrix of the clicked QC
	 */
	public void showCalibrationMatrix(int id) {
		int index = qcList.getQCPosition(id);
		JPanel panel = new JPanel();
		String[] items = qcList.getMatrix(index);
		JList list = new JList(items);
		panel.add(list);
		JOptionPane.showMessageDialog(null, panel, "Die Kalibrierungsmatrix", JOptionPane.PLAIN_MESSAGE);
	}
	
	/**
	 * Method used to initialize the positioning array
	 */
	public void initializePanelPos() {
		for (int i = 0; i < 50; i++) {
			panelpos[i] = -1;
		}
	}
	
	
	/**
	 * Method to get the next position equal to -1, used when creating a panel
	 * @return position
	 */
	public int nextPanelPos() {
		int position = 0;
		for (int i = 0; i < numberofPanels+1; i++) {
			if (panelpos[i] == -1) {
				position = i;
				panelpos[i] = i;
				break;
			}
		}
		numberofPanels++;
		return position;
	}
	
	
	/**
	 * Method to set the position pos to the default value of -1, used when deleting a panel
	 * @param pos
	 */
	public void decreasePanelPos(int pos) {
		panelpos[pos] = -1;
		numberofPanels--;
	}
	
	public void setNumberofPanels(int number) {
		numberofPanels = number;
	}
	
	public int getNumberofPanels() {
		return numberofPanels;
	}
	
}
