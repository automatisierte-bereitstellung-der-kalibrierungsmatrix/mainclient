package org.unistuttgart.acaliqu;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;

public class ClientTest {
	private static String url = "http://localhost:8080/MatrixServer/rest/qc";
	
	public WebTarget getWebTarget() {
		//Sets up the url and web target for the server
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		//For more specific resources (e.g. http://localhost:8080/MatrixServer/rest/qc/resource use:
		// WebTarget resourceTarget = webTarget.path("resource");
		return client.target(url);
	}
	
	public void testList() {
		WebTarget target = getWebTarget();
		WebTarget resourceTarget = target.path("/list");
		String response = resourceTarget.request().accept("application/json").get(String.class);
		
		System.out.println("Testing the getQCElements method");
		System.out.println(response);
	}
	
	public void testGet() {
		WebTarget target = getWebTarget();
		String id = "1";
		WebTarget resourceTarget = target.path("/ids/" + id);
		QCElement response = resourceTarget.request().accept("application/json").get(QCElement.class);
		
		System.out.println("Testing the get method");
		System.out.println(response);
	}
	
	public void testAdd() {
		WebTarget target = getWebTarget();
		WebTarget resourceTarget = target.path("/ids");
		String date = "2021-02-26 11:11:11";
		QCElement element = new QCElement(10, "dummyName", 10, date);
		Response response = resourceTarget.request().post(Entity.entity(element, "application/json"), Response.class);
		
		System.out.println("testing the add method");
		System.out.println(response.getLocation().toString());
	}
	
	public void testUpdate() {
		WebTarget target = getWebTarget();
		String id = "10";
		WebTarget resourceTarget = target.path("/ids/" + id);
		String date = "2021-02-26 11:11:11";
		QCElement element = new QCElement(10, "newName", 10, date);
		Response response = resourceTarget.request().put(Entity.entity(element, "application/json"), Response.class);
		
		System.out.println("testing the update method");
		System.out.println(response);
	}
	
	public void testDelete() {
		WebTarget target = getWebTarget();
		String id = "10";
		WebTarget resourceTarget = target.path("/ids/" + id);
		Response response = resourceTarget.request().delete(Response.class);
		
		System.out.println("testing the delete method");
		System.out.println(response);
	}
	
	public void testGetIds() {
		WebTarget target = getWebTarget();
		WebTarget resourceTarget = target.path("/list/ids");
		String response = resourceTarget.request().get(String.class);
		
		System.out.println("testing getIds method");
		System.out.println(response);
	}
	
}
